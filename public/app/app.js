var app = {};

var cities, stations, autocomplete;

var citiesXhr = new XMLHttpRequest();
    citiesXhr.open("GET", "data/cities.json");
    citiesXhr.onreadystatechange = function(res){
      //cities = JSON.parse(res.responseText);
      if (citiesXhr.readyState != 4) {
        return;
      }

      if (citiesXhr.status == 200) {
        cities = JSON.parse(citiesXhr.responseText);
      }
    };
    citiesXhr.send();


var stationXhr = new XMLHttpRequest();
    stationXhr.open("GET", "data/stations.json");
    stationXhr.onreadystatechange = function(res){
      //cities = JSON.parse(res.responseText);
      if (stationXhr.readyState != 4) {
        return;
      }

      if (stationXhr.status == 200) {
        stations = JSON.parse(stationXhr.responseText);
      }
    };
    stationXhr.send();

// var cityPairs = new XMLHttpRequest();
//     cityPairs.open("GET", "data/city-pairs.json");
//     cityPairs.setRequestHeader("X-Requested-With", "XMLHttpRequest");
//     cityPairs.onreadystatechange = function(res){
//       //cities = JSON.parse(res.responseText);
//       if (cityPairs.readyState != 4) {
//         return;
//       }
//
//       if (cityPairs.status == 200) {
//         city_pairs = JSON.parse(cityPairs.responseText);
//       }
//     };
//     cityPairs.send();
//
// var stationPairs = new XMLHttpRequest();
//     stationPairs.open("GET", "data/station-pairs.json");
//     stationPairs.setRequestHeader("X-Requested-With", "XMLHttpRequest");
//     stationPairs.onreadystatechange = function(res){
//       //cities = JSON.parse(res.responseText);
//       if (stationPairs.readyState != 4) {
//         return;
//       }
//
//       if (stationPairs.status == 200) {
//         station_pairs = JSON.parse(stationPairs.responseText);
//       }
//     };
//     stationPairs.send();

Translator = {
  trans: function(string){
    return string;
  }
}
var AUTOCOMPLETE_HASH = "hash"
setTimeout(function(){
  console.time("data");
  autocomplete = new AutoCompleteCollection({stations: stations, cities:cities});
  console.timeEnd("data");

  autocomplete.getDestinations("/data/city-pairs.json", "/data/station-pairs.json").done(function(city_pairs, station_pairs){
    console.time("inject");
    autocomplete.injectDestinations({stations: station_pairs, cities: city_pairs});
    console.timeEnd("inject");
  })

  $("#ArrivalPoint, #DeparturePoint").combobox({
    autocompleteCollection: autocomplete,
    autoFocus: true,
    delay    : 0,
    minLength: 0,
    position : {
        offset: '-1 4'
    },
    cases: {
      ArrivalPoint: {
        elementId: "ArrivalPoint",
        cityInput: $("#arrivalCity"),
        stationInput: $("#arrivalStation")
      },
      DeparturePoint: {
        elementId: "DeparturePoint",
        cityInput: $("#departureCity"),
        stationInput: $("#departureStation")
      }
    },
    select: function(){
    },
    open: function(){

    },

  })

},200);


app.RateCollection = Backbone.Collection.extend({
    url: "data/data.json",
    comparator: function (model) {
        return -model.get("points");
    },
    parse: function (response) {
        return response.records;
    },
});

app.IndexView = Backbone.View.extend({
    el: "#list",
    template: _.template($("#list_template").html()),
    initialize: function () {
        var _this = this;

        this.dataFetched = false;
        this.updatesCache = [];
        this.startVersion = 0;
        window.rateCollection = this.collection;

        var currentVersion = this.currentVersion = new Backbone.Model();

        this.updatesCollection = new Backbone.Collection();

        this.currentVersion.on("change", function(){
            _this.renderVersion();
        });

        this.collection.on("sync", function (collection, data) {
            _this.startVersion = data.version;
            _this.setVersion(data.version);
            _this.render();
        });

        this.collection.on("change", function () {
            this.sort();
            _this.render();
        });

        this.updatesCollection.on("add", function (collection, data) {
                var updates = this;
                    usedStack = [];
                updates.each(function(model){
                    var version =  currentVersion.get("version");
                    if(model.attributes.fromVersion == version){
                        _this.collection.set(model.attributes.updates, {
                            remove: false
                        });
                        _this.setVersion(model.attributes.toVersion);
                        _this.hightLightUpdates(model.attributes.updates);
                        usedStack.push(model);
                    }
                });

                updates.remove(usedStack);

        });

        this.ws();

    },
    render: function () {
        var _this = this;
         _this.$el.empty();
        _this.collection.each(function (model) {
            _this.$el.append(_this.template(model.attributes));
        });

    },
    renderVersion: function(){
        var time = (new Date(this.currentVersion.get("time"))).toLocaleTimeString()
        $("#version").html("Current version: <span class='version' > " + this.currentVersion.get("version") + "</span> on " +  time + " / " + this.startVersion)
          .find('.version').addClass('blink');

    },
    renderError: function(msg){
        this.$el.empty().append("<li class='error-msg'>"+msg+"</li>");
    },
    hightLightUpdates: function(updates){
        var _this = this;
        if(updates){
            updates.map(function(update){
                _this.$el.find("#member-"+ update.id)
                                    .addClass("updated");
            });
        }
    },
    ws: function () {
        var _this = this;
        var url = 'ws://localhost:3001';
        var ws = _this.webSocket = new WebSocket(url);

        ws.onmessage = function (e) {
            var data = JSON.parse(e.data);
            if (data.status && data.status == "CONNECTED") {
                _this.wsConnected();
            } else {
                _this.wsOnmessage(data);
            }
        };
        ws.onclose = function (e) {
            console.info("WebSocket was closed, reconnection... ", new Date());
            _this.wsClose();
            _this.ws();
        }
        ws.onerror = function (e) { _this.wsError(e); }
    },
    setVersion: function(version){
        this.currentVersion.set({version: version , time: (new Date()).getTime()});
    },
    wsOnmessage: function (data) {
            this.updatesCollection.add(data);
    },
    wsConnected: function () {
        var _this = this;

        this.collection.fetch({
            error: function(){
                _this.renderError("data was not retrieved");
            }
    });
    },
    wsError: function(e){
        console.log("ws.error", arguments);
        this.renderError("connection error, try later...");
        return (function(ws){
            if(typeof this.attempt == "undefined"){
                this.attempt = 1;
            } else {
                this.attempt++;
            };
            console.log(this.attempt);
            if(this.attempt == 3){
                ws.onclose = function(){};
            }
        })(e.target)
    },
    wsClose: function(e){
        this.collection.reset(null, {silent: true});
        this.updatesCollection.reset(null, {silent: true});
        this.setVersion(0);
        this.renderError("connection was closed wait for reconnection");
    }
})

var indexApp = new app.IndexView({
    collection: new app.RateCollection
})
