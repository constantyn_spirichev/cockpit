/**
 * Created by spk on 3/10/16.
 */

(function ($) {

    var translitMatchString = function (matchString) {
        matchString = matchString.replace(/ä/g, 'a')
            .replace(/Ä/ig, 'a')
            .replace(/Ö/ig, 'o')
            .replace(/Ü/ig, 'u')
            .replace(/ae/ig, 'a')
            .replace(/oe/ig, 'o')
            .replace(/ue/ig, 'u')
            .replace(/ss/g, 's')
            .replace(/ß/g, 's');
        return matchString;
    },

        normalizeMatchString = function (matchString) {
            // Taken from: https://codepoints.net/basic_multilingual_plane
            // ru locale has some wierd sequence of symbols
            var latin1SupplementSequence = "\u00C0-\u00FF",
                latinExtendedA = "\u0100-\u017F",
                latinExtendedB = "\u0180-\u024F",
                cyrillicSequence = "\u0400-\u04FF",
                cyrillicSupplementSequence = "\u0500-\u052F",
                lettersMatchSequence = latin1SupplementSequence + latinExtendedA + latinExtendedB + cyrillicSequence + cyrillicSupplementSequence,
                sanitizerRegex = "/[\w" + lettersMatchSequence + "]/g";

            return matchString.replace(sanitizerRegex, ' ');
        },

        getDestinations = function (cityUrl, stationUrl) {
            var city_pairs = new $.Deferred(),
                station_pairs = new $.Deferred(),
                savedHash = localStorage.getItem("autocomplete_hash");

            cityUrl = cityUrl || '/location/city-pairs';
            stationUrl = stationUrl || '/location/station-pairs';

            if (localStorage.getItem('city_pairs') && localStorage.getItem('station_pairs') && savedHash === AUTOCOMPLETE_HASH) {
                city_pairs.resolve(JSON.parse(localStorage.getItem('city_pairs')));
                station_pairs.resolve(JSON.parse(localStorage.getItem('station_pairs')));
            } else {
                $.get(cityUrl, function (data, status, jqXHR) {
                    city_pairs.resolve(data);
                    localStorage.setItem('city_pairs', jqXHR.responseText);
                });
                $.get(stationUrl, function (data, status, jqXHR) {
                    station_pairs.resolve(data);
                    localStorage.setItem('station_pairs', jqXHR.responseText);
                });
                localStorage.setItem('autocomplete_hash', AUTOCOMPLETE_HASH);
            }
            return $.when(city_pairs, station_pairs);
        };

    var AutoCompleteModels = function (data) {

        this.models = [];
        this._byId = data || {};

        if (typeof data !== "undefined") {
            _.each(data, function (item, index) {
                this.models.push(item);
            }, this);

            this.models = _.sortBy(this.models, 'name');
        }

    };

    AutoCompleteModels.prototype.getById = function (id) {
        return this._byId[id] || null;
    };

    AutoCompleteModels.prototype.addModel = function (data) {
        if (typeof data === "undefinded" || typeof data.id === "undefinded") {
            console.error(new Error("nothing to add or id attribute is missing"));
            return;
        }
        this._byId[data.id] = data;
        this.models.push(data);

        //this.models = _.sortBy(this.models, 'name');
    };

    AutoCompleteModels.prototype.seekItems = function (start, number) {
        var result = new AutoCompleteModels();

            start = start || 0;
            number = number ? (number + start) : this.models.length;

        for (var i = start || 0; i < number; i++) {
            if (this.models[i]) {
                result.addModel(this.models[i]);
            } else {
                break;
            }
        }
        return result;
    };


    var AutoCompleteCollection = window.AutoCompleteCollection = function (data) {

        this.cities = data ? new AutoCompleteModels(data.cities) : new AutoCompleteModels();
        this.stations = data ? new AutoCompleteModels(data.stations) : new AutoCompleteModels();

        this.setLength();

        data && this._setMatchString();
    };

    AutoCompleteCollection.prototype.setLength = function () {
        this.length = this.cities.models.length + this.stations.models.length;
    };

    AutoCompleteCollection.prototype._setMatchString = function () {

        _.map(this.stations.models, function (item, index) {
            var translited;

            item.matchString = item.name + " " + item.aliases + " ";
            translited = translitMatchString(item.matchString);
            item.matchString += translited == item.matchString ? "" : translited;
            item.matchString = normalizeMatchString(item.matchString);

        }, this);

        _.map(this.cities.models, function (item, index) {
            var translited;

            item.matchString = item.name + " " + item.aliases;
            translited = translitMatchString(item.matchString);
            item.matchString += translited == item.matchString ? "" : translited;
            item.matchString = normalizeMatchString(item.matchString);

            _.map(this.getStationsInCity(item.id).models, function (station) {
                item.matchString += " " + station.matchString;
            });
        }, this);
    };

    AutoCompleteCollection.prototype.getStationsInCity = function (id) {
        var city = this.cities._byId[id] || {};
        var stations = {};

        if (city.stations && city.stations.length) {
            _.map(city.stations, function (item) {
                if (this.stations._byId[item]) {
                    stations[item] = this.stations._byId[item];
                }
            }, this);
        }
        return new AutoCompleteModels(stations);
    };

    AutoCompleteCollection.prototype.search = function (pattern, propertie) {
        var result = new AutoCompleteCollection();
        var regExp = typeof pattern == "object" ? pattern : new RegExp(pattern, "i");
        var matchProp = propertie || "matchString";


        _.map(this.cities.models, function (item) {
            if (pattern.test(item[matchProp] || item.matchString)) {
                result.cities.addModel(item);
            }
        }, this);

        _.map(this.stations.models, function (item) {
            if (pattern.test(item[matchProp] || item.matchString)) {
                result.stations.addModel(item);
            }
        }, this);

        result.setLength();

        return result;
    };

    AutoCompleteCollection.prototype.injectDestinations = function (data) {
        if (data && data.cities && data.stations) {
            this.constructor.prototype.destinations = {
                cities: new AutoCompleteModels(data.cities),
                stations: new AutoCompleteModels(data.stations)
            };
        } else {
            console.error("destinations wasn't injected: not valid data");
        }
    };

    AutoCompleteCollection.prototype.filterDestination = function (cityId, stationId) {
        // @TODO add few combinations of expresions
        // for passed Id's to keep consistance
        if (this.destinations && cityId) {
            var result = new AutoCompleteCollection();
            var toCities = this.destinations.cities.getById(cityId).destinations;
            var toStations = stationId ? this.destinations.stations.getById(stationId).destinations : null;

            _.map(toCities, function (item) {
                var city = this.cities.getById(item);

                    city !== null && result.cities.addModel(city);

            }, this);

            if (toStations !== null) {
                _.map(toStations, function (item) {
                    var station = this.stations.getById(item);

                        station !== null && result.stations.addModel(station);

                }, this);

            } else {
                result.stations = this.stations;
            }

        } else {
            return this;
        }

        result.cities.models = _.sortBy(result.cities.models, 'name');
        result.stations.models = _.sortBy(result.stations.models, 'name');

        result.setLength();

        return result;
    };

    AutoCompleteCollection.prototype.getDestinations = getDestinations;

})(jQuery);

;(function ($) {
    if ($.widget) {
        $.widget('custom.combobox', $.ui.autocomplete, {
            options: {
                autocompleteCollection: {}
            },

            pattern: "",

            arrivalCityId: null,
            arrivalStationId: null,
            departureCityId: null,
            departureStationId: null,

            currentValue: null,
            renderCacheTimout: null,

            _init: function () {
                console.log("init with", this.element)
                var wrapper = $('<span class="ui-combobox"></span>'),
                    _this = this;

                this.element
                    .addClass('ui-combobox-input')
                    .addClass('ui-state-default')
                    .wrap(wrapper);


                this.opposite = _.reject(this.options.cases, function (item) {
                    return item.elementId == this.element[0].id
                }, this)[0];

                this._setInitValues();
                this._setEventListeners();
                this._appendResetButton();

            },
            _initSource: function () {
                var _this = this;
                this.source = function (request, response) {
                    console.time("source");
                    var term = $.trim(request.term).replace(/[\-\[\]{}()*+?.,\\\^$|#\s]/g, "\\$&"),
                        pattern = new RegExp('^' + term + '| ' + term + '|-' + term + '|' + term + '|\\(' + term + '|' + term + '\\)', 'i'),
                        opposite,
                        cityId,
                        stationId,
                        outData = this.options.autocompleteCollection;

                    this.pattern = term;

                    outData = term === '' ? outData : outData.search(pattern);

                    console.time("dest")

                    if (outData.destinations && _this.element[0].id == _this.options.cases.ArrivalPoint.elementId) {

                        cityId = this.opposite.cityInput.val();
                        stationId = this.opposite.stationInput.val();

                        outData = cityId ? outData.filterDestination(cityId, stationId) : outData;
                    }
                    console.timeEnd("dest")


                    console.timeEnd("source");

                    response(outData);


                };
            },

            _setInitValues: function () {
                var cases = this.options.cases,
                    currentCase = cases[this.element[0].id],
                    value;

                var stationId = currentCase.stationInput.val();
                var cityId = currentCase.cityInput.val();

                if (stationId) {
                    var station = this.options.autocompleteCollection.stations.getById(stationId);

                    value = station.name;

                } else if (cityId) {
                    var city = this.options.autocompleteCollection.cities.getById(cityId);

                    value = city.name;
                }

                this._value(value);

            },

            _updateValues: function (item) {

                var cases = this.options.cases,
                    currentCase = cases[this.element[0].id];

                if (item.cityId) {
                    currentCase.cityInput.val(item.cityId);
                    currentCase.stationInput.val(item.id);
                } else {
                    currentCase.cityInput.val(item.id);
                    currentCase.stationInput.val('');
                }

            },

            _setEventListeners: function () {
                var _this = this;

                this.element.on('click', function () {
                        _this._search('');

                        if (this.setSelectionRange) {
                            this.setSelectionRange(0, this.value.length);
                        } else if (this.select) {
                            this.select();
                        }

                    })
                    .on("blur", function () {
                        console.log("selectedItem: ", _this.selectedItem)
                        console.log("previous: ", _this.previous)
                        console.log("term: ", _this.term)
                        if (this.value === '' || !_this.selectedItem && this.value !== _this.previous) {
                            console.log("set init value")
                            _this._setInitValues();
                        }
                    })
                    .on("comboboxopen", function () {

                        $(this).parent().addClass("opened reset-button-visible");

                        _this.resetButton.show();
                        // scroll to current value
                        _this._scrollToCurent();
                    })
                    .on("comboboxclose", function () {
                        $(this).parent().removeClass("opened reset-button-visible");
                        _this.resetButton.hide();

                    })
                    .on("comboboxselect", function (event, ui) {
                        _this._updateValues(ui.item);
                    });

            },
            _appendResetButton: function () {
                var resetButton = this.resetButton = $('<span class="combobox-reset glyphicon glyphicon-remove-circle"></span>'),
                    _this = this;


                _this.element
                    .closest('.ui-combobox')
                    .append(resetButton);

                resetButton
                    .on('mousedown', function (event) {
                        event.preventDefault();
                        _this.element
                            .focus().val('');
                        _this.element.trigger("change");

                    }).hide();
            },
            _renderCategory: function (category) {
                var el;

                el = $('<li/>', {class: "ui-autocomplete-category"}).text(category);

                return el;
            },
            _renderItem: function (item) {
                var el,
                    html,
                    className = 'ui-menu-item ',
                    currentValue = this.element.val();

                if (item.cityId) {
                    className += 'item-nested';
                }

                if (item.name === currentValue) {
                    className += ' selected';
                }

                el = $('<li/>', {class: className});
                el.data('ui-autocomplete-item', {value: item.name, id: item.id, cityId: item.cityId || null});

                html = '<a>' + item.label.replace(new RegExp('(' + this.pattern + ')', 'i'), "<strong>$1</strong>");
                html += (item.stations && item.stations.length) ? '<span> (' + Translator.trans('all stations') + ') </span>' : "";
                html += '</a>';

                el.html(html);

                return el;
            },
            _renderMenu: function (ul, items) {
                // @TODO remove repeated parts of code

                var _this = this,
                    currentCategory = '',
                    stack = [],
                    perPage = 10,
                    currentPage = 0,
                    selected,
                    startIndex,
                    endIndex;

                var scrollHandler = (function () {
                    var initValue = 0;
                    var bounceTimer = null;

                    return function (e) {
                        if (!bounceTimer) {
                            bounceTimer = setTimeout(function bounce() {

                                var $el = $(e.target),
                                    currentValue = $el.scrollTop(),
                                    direction = currentValue - initValue;

                                if (direction > 0 && currentValue >= e.target.scrollHeight - 440) {
                                    console.log("down");
                                    startIndex = currentPage * perPage;
                                    endIndex = (currentPage * perPage) + perPage;
                                    ul.append(stack.slice(startIndex, endIndex < 0 ? stack.length - 1 : endIndex));
                                    currentPage++;
                                }
                                // @TODO make smoose parsing elements
                                // if(direction < 0 && currentValue <= 100 && currentPage > 0){
                                //   console.log("up");
                                //   ul.prepend(stack.slice((currentPage * perPage) - perPage, (currentPage * perPage)))
                                //
                                //   currentPage--;
                                // }

                                initValue = currentValue;
                                bounceTimer = null;

                            }, 250);
                        }
                    };

                })();


                ul.off("scroll", scrollHandler);


                $.each(items.cities.models, function (cityIndex, city) {

                    if (city.category && city.category != currentCategory) {
                        stack.push(_this._renderCategory(city.category));
                        currentCategory = city.category;
                    }


                    stack.push(_this._renderItem(city));
                    selected = stack[stack.length - 1].hasClass("selected") ? stack.length - 1 : selected;

                    $.each(city.stations, function (stationIndex, stationId) {
                        var station = items.stations.getById(stationId);

                        if (station) {
                            stack.push(_this._renderItem(station));
                            selected = stack[stack.length - 1].hasClass("selected") ? stack.length - 1 : selected;
                        }
                    });

                });

                if (selected) {
                    currentPage = Math.floor(selected / perPage);
                }

                startIndex = 0;
                endIndex = (currentPage * perPage) + perPage;

                ul.append(stack.slice(startIndex, endIndex < 0 ? stack.length - 1 : endIndex));

                currentPage++;

                ul.on("scroll", scrollHandler);
            },

            _search: function (value, prop) {
                this.pending++;
                this.element.addClass("ui-autocomplete-loading");
                this.cancelSearch = false;

                this.source({term: value, prop: prop}, this._response());
            },
            __response: function (content) {
                console.time("response");
                this._super(content);
                console.timeEnd("response");
                //this._deactivateLoader()
            },
            _suggest: function (items) {
                var ul = this.menu.element
                    .empty()
                    .zIndex(this.element.zIndex() + 1);


                //this.menu.refresh();
                console.time("menu show");
                ul.show();
                console.timeEnd("menu show");


                console.time("menu render");
                this._renderMenu(ul, items);
                console.timeEnd("menu render");

                this._setMenuPosition();


                if (this.options.autoFocus) {
                    this.menu.next();
                }
            },
            _scrollToCurent: function () {
                var ul = this.menu.element,
                    selectedEl = ul.find(".selected");
                if (selectedEl.offset() && ul.scrollTop() + selectedEl.offset().top > ul.height()) {
                    ul.scrollTop(ul.scrollTop() + selectedEl.offset().top - ul.height() * 1);
                }
            },
            _setMenuPosition: function () {
                var ul = this.menu.element;
                ul.position($.extend({
                    of: this.element
                }, this.options.position));
            },
            _normalize: function (content) {
                return content;
            },
            _resetValues: function () {

                this.element.val('');

            }


        });
    }

})(jQuery);
