var express = require('express');
var ws = require("nodejs-websocket");
var fs = require('fs');
var spawn = require('child_process').spawn;
var exec = require('child_process').exec;
var app = express();

app.use(express.static('public'));

var  wsMessageHandler = function(message){
  /**
  * {action: string, params: {}}
  *
  */
  //var data = JSON.parse(message);
  var wsConnection = this;
  var parsedMessage = JSON.parse(message);
  var command = parsedMessage.action;
  var params = parsedMessage.params || [];
  var fileName = parsedMessage.fileName;
  var data = JSON.stringify(parsedMessage.data, null, " ");
  //var run = spawn(command, params);

  if (command == "save" && fileName) {
    wsConnection.sendText("data to save resived");
    console.log("data to save resived");

    fs.writeFile("public/data/"+fileName, data, function(err){
      if (err) {
        wsConnection.sendText("file wasn't save");
        throw err;
      }
      wsConnection.sendText("file saved");
      console.log("file save");
    })
  }

    //wsConnection.sendText(text);

};

app.listen(3000, function () {
  console.log('Example app listening on port 3000!');
  var server = ws.createServer(function (conn) {
    console.log("New connection")
    conn.sendText("{\"status\": \"CONNECTED\"}");
    conn.on("text", wsMessageHandler);
    conn.on("close", function (code, reason) {
      console.log("Connection closed")
    })
  }).listen(3001);

});
